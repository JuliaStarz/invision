$(function() {
    /** form validation */
    $('#phone, #vacancy-phone, #authorsForm-phone').mask('+7(000)000-00-00', {placeholder: '+7(___)___-__-__'});
    
    // feature detection for drag&drop upload
    var isAdvancedUpload = function() {
        var div = document.createElement( 'div' );
        return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
    }();

    // applying the effect for every form
    var form         = document.getElementById('vc-form');
    if(form) {
        var input		 = document.querySelector('input[type="file"]'),
            label		 = document.querySelector('.vc-filename'),
            errorMsg	 = document.querySelector('.box__error span'),
            restart		 = document.querySelectorAll('.box__restart'),
            droppedFiles = false,
            showFiles	 = function(files) {
                label.textContent = files[0].name;
            };

        input.addEventListener('change', function( e ) {
            showFiles( e.target.files );
        });

        // drag&drop files if the feature is available
        if( isAdvancedUpload )
        {
            form.classList.add('has-advanced-upload'); // letting the CSS part to know drag&drop is supported by the browser

            [ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event )
            {
                form.addEventListener( event, function( e )
                {
                    // preventing the unwanted behaviours
                    e.preventDefault();
                    e.stopPropagation();
                });
            });
            [ 'dragover', 'dragenter' ].forEach( function( event )
            {
                form.addEventListener( event, function()
                {
                    form.classList.add( 'is-dragover' );
                });
            });
            [ 'dragleave', 'dragend', 'drop' ].forEach( function( event )
            {
                form.addEventListener( event, function()
                {
                    form.classList.remove( 'is-dragover' );
                });
            });
            form.addEventListener( 'drop', function( e )
            {
                droppedFiles = e.dataTransfer.files; // the files that were dropped
                showFiles( droppedFiles );
            });
        }

        // Firefox focus bug fix for file input
        input.addEventListener('focus', function(){ input.classList.add('has-focus')});
        input.addEventListener('blur', function(){ input.classList.remove('has-focus')});
    }
    window.addEventListener('load', function() {
        $('.needs-validation').on('submit', function(e){
            e.preventDefault();
            var $form = $(this);
            var errors = [];

            if ($form.hasClass('is-uploading')) return false;
          
            $form.addClass('is-uploading').removeClass('is-error');

            var emailTest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            $(this).find('input, select, textarea').each(function() {
                $(this).removeClass('is-invalid');
                if(this.required && 
                    (this.value.trim() == '' ||
                    (this.type == 'tel' && this.value.replace(/[)+(_-]/g, '').length < 11) ||
                    (this.type == 'email' && !emailTest.test(this.value)))
                ) {
                    errors.push(this.name);
                    $(this).addClass('is-invalid');
                }
            });

            $form.addClass('was-validated');

            if(!errors.length) {
                if (isAdvancedUpload) {
                    var ajaxData = new FormData($form[0]);

                    if (droppedFiles) {
                        $.each( droppedFiles, function(i, file) {
                            ajaxData.append( input.attr('name'), file );
                        });
                    }

                    $.ajax({
                        url: $form.attr('action'),
                        type: $form.attr('method'),
                        data: ajaxData,
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false,
                        complete: function() {
                            $form[0].reset();
                            $form.removeClass('was-validated', 'is-uploading');
                            $('#thx').modal('show');
                            $('#form').modal('hide');
                        },
                        success: function(data) {
                            $form.addClass( data.success == true ? 'is-success' : 'is-error' );
                            if (!data.success) $errorMsg.text(data.error);
                        }
                    });
                } else {
                    $.ajax({
                        type: $form.attr('method'),
                        url: $form.attr('action'),
                        data: $form.serialize(),
            
                        complete: function(data, status) {
                            $form[0].reset();
                            $form.removeClass('was-validated', 'is-uploading');

                            $('#thx').modal('show');
                            $('#form').modal('hide');
                        }
                    });
                }
            }
        });
    }, false);

    $('.asc, .desc').on('click', function(){
        var id = $(this).attr('id');

        $(this).toggleClass('asc desc');
        var val = $(this).attr('class');
        $('input[name="' + id + '"]').val(val);
    })

    $(".totop").click(function(e){
        e.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
    });

    $('.main').on('click', '.tab-item', function(){
        var index = $(this).index();

        $(this).addClass('current');
        $(this).siblings().removeClass('current');

        var el = $(this).parents('.tabs').find('.tab-content').eq(index);
        el.show();
        el.siblings().hide();

        if($(this).parents('.tabs').hasClass('switch-tabs')) {
            activateSlider(el);
        }
        if($(this).parents('.tabs').hasClass('tab-slider')) {
            var slider = $(this).parents('.tabs').find('.tab-contents-slider');
            activateSlide(slider, index);
        }
    });
    $('.tab-contents-slider').on('init', function(event, slider) {
        $(slider.$slides).each(function(index, el){
            $(el).find('.tab-contents-counter').text((index + 1) + '/' + slider.slideCount);
        })
    })
    var slider = $('.tab-contents-slider').slick({
        slidesToShow: 1,
        arrows: $('.tab-contents-slider').data('arrows'),
        adaptiveHeight: true
    });    
    $('.tab-contents-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $(this).parents('.tabs').find('.tab-item:nth-child(' + (nextSlide + 1) + ')').click();
    });
    $('body').on('click', '.slider-next', function(){
        slider.slick('slickNext');
    });
    $('body').on('click', '.slider-prev', function(){
        slider.slick('slickPrev');
    });

    function activateSlide(slider, index) {
        if($(slider).slick('slickCurrentSlide') !== index)
            $(slider).slick('slickGoTo', index);
    }

    function activateSlider(el) {
        $('.tabs-slider').each(function(){
            if($(this).hasClass('slick-initialized'))
                $(this).slick('unslick');
        });
        el.find('.tabs-slider').slick({
            slidesPerRow: 5,
            dots: true
        });
    }

    $('.tab-list').each(function(){
        $(this).find('.tab-item').first().click();
    });

    $('.main').on('click', '.switch-content', function(){
        var id = $(this).data('id');

        $(this).addClass('current');
        $(this).siblings().removeClass('current');

        var el = $('#' + id);
        el.show();
        el.siblings().hide();
    });

    $('.h2').each(function(){
        $(this).find('.switch-content').first().click();
    });

    $(window).scroll(function(){
        var firstScreen = ($('.first-screen').length) ? $('.first-screen').outerHeight() : $('.header').outerHeight();
        var scroll = $(this).scrollTop();

        if(scroll > firstScreen)
            $('.header').addClass('position-fixed');
        else
            $('.header').removeClass('position-fixed');
    })

    
    /* main page slider */
    if(document.getElementById('mainpage-slider')) {
        $('.main-slider').slick({
            arrows: false,
            slidesToShow: 1,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 5000,
            asNavFor: '.main-slider-tabs',
            dots: true,
            appendDots: $('.main-slider-dots'),
            customPaging: function(slider, pageIndex) {
                return $('<button></button').text($(slider.$slides[pageIndex]).find('.main-slide').data('buttonlabel'));
            }
        });
        var tabs = $('.main-slider-tabs').slick({
            slidesToShow: 1,
            asNavFor: '.main-slider',
            focusOnSelect: true,
            fade: true,
            cssEase: 'linear'
        });
        $('body').on('click', '.main-slider-tabs-img img', function(){
            tabs.slick('slickNext');
        });
    }
    if(document.getElementById('reviews-slider')) {
        $('.reviews-contents').slick({
            slidesToShow: 1,
            asNavFor: '.reviews-tabs',
            adaptiveHeight: true
        });
        $('.reviews-tabs').slick({
            slidesToShow: 5,
            centerMode: true,
            centerPadding: '60px',
            asNavFor: '.reviews-contents',
            arrows: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        centerPadding: '30px',
                    }
                }
            ]
        });
    }
    if(document.getElementById('publish-reviews')) {
        $('#publish-reviews').on('init', function(event, slider) {
            $(slider.$slides).each(function(index, el){
                $(el).find('.publish-review-num').text((index + 1) + '/' + slider.slideCount);
            })
        })
        $('#publish-reviews').slick({
            slidesToShow: 1,
            adaptiveHeight: true
        });
    }
    if($(window).outerWidth() >= 768) {
        $('.slider-content').each(function(){
            $(this).niceScroll({
                cursorcolor: "#F3A005",
                cursorwidth: "7px",
                cursorborderradius: "0px",
                railpadding: { top: 0, right: 0, left: -15, bottom: 0 },
                horizrailenabled: false,
                autohidemode: 'scroll'
            });
        });
    }
})